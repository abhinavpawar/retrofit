package com.assignment6.retrofit.entities;

/**
 * Created by click on 7/20/15.
 */
public class User {
    private String id;
    private String name;

    private String username;

    private String email;
    private address address;
    static class address {
        private String street;
        private String suite;
        private String city;
        private String zipcode;

        @Override
        public String toString() {
            return  "Street-" + street + "\n" +
                    "Suite-" + suite + "\n" +
                    "City-" + city + "\n" +
                    "Zipcode-" + zipcode + "\n"
                    ;
        }
    }


    private String phone;
    private String website;

    public String getId() {
        return this.id;
    }

   public String getName() { return this.name; }
   // public String getUsername() { return this.username; }
   // public String getEmail() { return this.email; }
   // public String getPhone (){ return this.phone; }
   // public String getWebsite() { return this.website; }


    @Override
    public String toString() {
        return
                "Id-" + id + "\n" +
                "Name-" + name +  "\n" +
                "Username-" + username + "\n" +
                "Email-" + email +  "\n" +
                "Phone-" + phone + "\n" +
                "Website-" + website +  "\n" +
                "Address-" + address.toString() + "\n"
                ;
    }
}
