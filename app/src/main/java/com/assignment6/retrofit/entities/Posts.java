package com.assignment6.retrofit.entities;

/**
 * Created by click on 7/21/15.
 */
public class Posts {
    private String id;
    private String title;
    private String body;

    public String getBody() {
        return body;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
