package com.assignment6.retrofit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.assignment6.retrofit.entities.User;
import com.assignment6.retrofit.util.MyBaseAdapter;
import com.assignment6.retrofit.util.WebService;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity {
    ArrayList<User> userList = new ArrayList<User>();
    MyBaseAdapter adapter;
    final String END_POINT = "http://jsonplaceholder.typicode.com/";
    ListView listView;
    TextView userTextBox;
    int userPosition;
    ImageView imageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userTextBox = (TextView) findViewById(R.id.userID);
        imageView = (ImageView) findViewById(R.id.userIcon);
        listView = (ListView) findViewById(R.id.list);
        adapter = new MyBaseAdapter(this, userList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                imageView.setImageResource(R.mipmap.ic_launcher);
                userTextBox.setText(adapter.getItem(position).toString());
                userPosition = position;

            }
        });
    }

    public void getPostsButton(View v) {
        Intent intent = new Intent(this, UserPosts.class);
        String id = adapter.getItem(userPosition).getId();
        intent.putExtra("userId", id);
        startActivityForResult(intent,1);
       // finish();


    }

    public void getUsersButton(View v) {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(END_POINT).build();
        WebService webService = restAdapter.create(WebService.class);
        webService.listUsers(new Callback<ArrayList<User>>() {
            @Override
            public void success(ArrayList<User> users, Response response) {
                userList.addAll(users);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

}