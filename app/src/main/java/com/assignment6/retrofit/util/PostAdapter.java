package com.assignment6.retrofit.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment6.retrofit.R;
import com.assignment6.retrofit.entities.Posts;
import com.assignment6.retrofit.entities.User;

import java.util.ArrayList;

public class PostAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<Posts> mUserPosts;

    public PostAdapter(Context context, ArrayList<Posts> userPosts) {
        mInflater = LayoutInflater.from(context);
        mUserPosts = userPosts;
    }

    @Override
    public int getCount() {
        return mUserPosts.size();
    }

    @Override
    public Posts getItem(int position) {

        return mUserPosts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_student, parent, false);
            holder = new ViewHolder();

            holder.name = (TextView) view.findViewById(R.id.labelName);
            holder.id = (TextView) view.findViewById(R.id.labelRoll);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        Posts posts = getItem(position);


        holder.name.setText(posts.getBody());

        holder.id.setText(posts.getTitle());

        return view;
    }

    private class ViewHolder {

        public TextView name;
        public TextView id;
    }


}

