package com.assignment6.retrofit.util;

import com.assignment6.retrofit.entities.Posts;
import com.assignment6.retrofit.entities.User;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by click on 7/20/15.
 */
public interface WebService {

    @GET("/users")
    void listUsers(Callback<ArrayList<User>> response);

    @GET("/posts")
    void listPosts(@Query("userId") int userId, Callback<ArrayList<Posts>> response);
}
