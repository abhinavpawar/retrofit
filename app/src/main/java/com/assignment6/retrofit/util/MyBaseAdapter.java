package com.assignment6.retrofit.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment6.retrofit.R;

import com.assignment6.retrofit.entities.User;

import java.util.ArrayList;

public class MyBaseAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<User> mUser;

    public MyBaseAdapter(Context context, ArrayList<User> user) {
        mInflater = LayoutInflater.from(context);
        mUser = user;
    }

    @Override
    public int getCount() {
        return mUser.size();
    }

    @Override
    public User getItem(int position) {

        return mUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_student, parent, false);
            holder = new ViewHolder();
            holder.avatar = (ImageView) view.findViewById(R.id.icon);
            holder.name = (TextView) view.findViewById(R.id.labelName);
            holder.id = (TextView) view.findViewById(R.id.labelRoll);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        User user = getItem(position);

        holder.avatar.setImageResource(R.mipmap.ic_launcher);


        holder.name.setText(user.getName());

        holder.id.setText(user.getId());

        return view;
    }

    private class ViewHolder {
        public ImageView avatar;
        public TextView name;
        public TextView id;
    }


}

