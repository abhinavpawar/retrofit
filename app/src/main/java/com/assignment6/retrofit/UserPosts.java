package com.assignment6.retrofit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.ListView;

import com.assignment6.retrofit.entities.Posts;
import com.assignment6.retrofit.entities.User;
import com.assignment6.retrofit.util.PostAdapter;
import com.assignment6.retrofit.util.WebService;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by click on 7/21/15.
 */
public class UserPosts extends Activity {
    final String END_POINT = "http://jsonplaceholder.typicode.com/";
    ListView listView;
    ArrayList<Posts> userArrayList = new ArrayList<Posts>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_posts_layout);
        listView = (ListView) findViewById(R.id.postsList);
        final PostAdapter adapter = new PostAdapter(this, userArrayList);
        listView.setAdapter(adapter);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(END_POINT).setLogLevel(RestAdapter.LogLevel.FULL).build();
        WebService webService = restAdapter.create(WebService.class);
        Intent intent = getIntent();
        int userId = Integer.parseInt(intent.getExtras().getString("userId"));

        webService.listPosts(userId, new Callback<ArrayList<Posts>>() {
            @Override
            public void success(ArrayList<Posts> postses, Response response) {
                userArrayList.addAll(postses);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


    }
}
